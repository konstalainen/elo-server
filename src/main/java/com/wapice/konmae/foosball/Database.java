package com.wapice.konmae.foosball;

import java.util.*;
import java.util.Map.Entry;
import java.util.function.Function;

import com.wapice.konmae.foosball.connectors.ConnectionFactory;
import com.wapice.konmae.foosball.connectors.IDatabaseConnector;
import com.wapice.konmae.foosball.models.*;

import xyz.konstamaenpanen.logger.LogManager;
import xyz.konstamaenpanen.logger.Logger;

public class Database {
		
	public static final Logger logger = LogManager.getLogger(Main.class.getName());
	private static Database singleton = null;
	
	private IDatabaseConnector connection = null;
	private final Map<Integer, User> users;
	private final Map<Integer, Match> matches;
	
	private Database() {
		
		logger.log("Getting the database connector");
		
		try {
			
			this.connection = ConnectionFactory.create(Utils.getSetting("db-connector"));
			
		} catch(Exception e) {
			logger.log(e);
			System.exit(1);
		}
				
		logger.log("Connected!");
				
		users = this.connection.getUsers();
		matches = this.connection.getMatches(this.users);
		logger.log("I have " + users.size() + " users and " + matches.size() + " matches initially in the database");
		
	}
	
	private Database(Database db) {
		
		this.connection = db.connection;
		this.users = this.connection.getUsers();
		this.matches = this.connection.getMatches(this.users);
		
		logger.log("DB reloaded.");
		logger.log("I have " + users.size() + " users and " + matches.size() + " matches in the database");
		
	}
		
	public static synchronized Database get() {
		
		if(singleton == null) {
			singleton = new Database();
		}
		
		return singleton;
		
	}
	
	public static synchronized void reload() {
		
		if(singleton == null) {
			singleton = new Database();
			return;
		}
		
		singleton = new Database(singleton);
		
	}

	public List<User> getUsers() {
		
		return new ArrayList<>(this.users.values());
		
	}
	
	public List<User> getUsers(String order, int limit) {
		
		Map<Integer, User> users = new LinkedHashMap<>();
		List<User> list = new ArrayList<>(this.users.values());
		Comparator<? super User> comparator = User.getComparator(order);
		
		if(comparator != null) {
			Collections.sort(list, comparator);
			//list.sort(comparator);
		}
		
		if(limit > 0 && limit <= list.size()) {
			list = list.subList(0, limit);
		}
		
		for(User u : list) {
			users.put(u.id, u);
		}
		
		return list;
		
	}
	
	public User getUser(int id) {
		
		return this.users.get(id);
		
	}
	
	public User getUser(String name) {
		
		if(name != null) {
			
			Iterator<Entry<Integer, User>> i = this.users.entrySet().iterator();
			
			while(i.hasNext()) {
				
				User u = i.next().getValue();
				
				if(u.nameEquals(name))
					return u;
				
			}
			
			return null;
			
		}
		
		else {
			return null;
		}
		
	}
	
	public int putUser(String name) throws IllegalArgumentException {
		
		if(this.getUser(name) != null)
			throw new IllegalArgumentException("User with name " + name + " already exists.");
		
		int userId = this.connection.putUser(name);
		
		if(userId >= 0) {
			
			this.users.put(userId, new User(name, userId));
			return userId;
			
		}
		else {
			return -1;
		}
		
					
	}
	
	public void putMatch(Match m) {
		
		int id = this.connection.putMatch(m);
		m.id = id;
		this.matches.put(id, m);
				
	}
		
	public List<Match> getMatches() {
		
		List<Match> m = new ArrayList<>(this.matches.values());
		
		Comparator<? super Match> comparator = new Comparator<Match>() {
			@Override
			public int compare(Match m1, Match m2) {
				return Integer.compare(m1.id, m2.id);
				//return (-1) * Double.compare(u1.points, u2.points);
			}
		};
		
		Collections.sort(m, comparator);
		
		return m;
		
	}
	
	public List<Match> getMatches(Function<Match, Boolean> filter) {
		List<Match> m = new ArrayList<>();
		List<Match> temp = new ArrayList<>(this.matches.values());
		
		for(int i = 0; i < temp.size(); i++) {
			Match match = temp.get(i);
			
			if (filter.apply(match)) {
				m.add(match);
			}
		}
		
		return m;
	}
	
	
}
