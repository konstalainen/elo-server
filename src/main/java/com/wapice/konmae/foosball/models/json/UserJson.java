package com.wapice.konmae.foosball.models.json;

import com.google.gson.annotations.Expose;

public class UserJson {

	
	@Expose
	public String name = null;
	
	@Expose
	public String key = null;
	
	@Override
	public String toString() {
		
		return this.name;
		
	}
}
