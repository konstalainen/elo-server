package com.wapice.konmae.foosball.models.json;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.wapice.konmae.foosball.Utils;

public class MatchJson {
	
	@Expose
	public int[] homePlayers = null;
	@Expose
	public int[] awayPlayers = null;
	
	@Expose
	public String homePlayerNames[] = null;
	@Expose
	public String awayPlayerNames[] = null;
	
	@Expose 
	public int goals1 = -1;
	@Expose
	public int goals2 = -1;
	@Expose
	public String key = null;
	
	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("[");
		for(int i = 0; i < this.homePlayers.length; i++) {
			
			if(i > 0) {
				sb.append(", ");
			}
			
			sb.append(this.homePlayers[i]);
		}
		
		sb.append("] ");
		sb.append(this.goals1);
		sb.append(" – ");
		sb.append(this.goals2);
		sb.append("[ ");
		
		for(int i = 0; i < this.awayPlayers.length; i++) {
			
			if(i > 0) {
				sb.append(", ");
			}
			
			sb.append(this.awayPlayers[i]);
		}
		
		sb.append("] {");
		sb.append(this.key);
		sb.append("}");
		
		return sb.toString();
		
	}
	
	public boolean useNames() {
		return !Utils.isNullOrEmpty(this.homePlayerNames) && !Utils.isNullOrEmpty(this.awayPlayerNames);
	}
	
	public boolean checkKey() {
		return Utils.validateMatchChecksum(this);
	}

	public String getError() {
		
		boolean useNames = useNames();
				
		if(Utils.isNullOrEmpty(this.homePlayers) && !useNames) {
			return "Player id for home team not set";
		}
		if(Utils.isNullOrEmpty(this.awayPlayers) && !useNames) {
			return "Player id for away team not set";
		}
		if(!Utils.isNullOrEmpty(this.homePlayers) && !Utils.isNullOrEmpty(this.awayPlayers) && 
				(!Utils.isNullOrEmpty(this.homePlayerNames) || !Utils.isNullOrEmpty(this.awayPlayerNames))) {
			return "Use either the player id or player names";
		}
		if(Utils.isNullOrEmpty(this.homePlayerNames) && useNames) {
			return "Names for home team not set";
		}
		if(Utils.isNullOrEmpty(this.awayPlayerNames) && useNames) {
			return "Names for away team not set";
		}
		if(this.goals1 < 0) {
			return "Goals for home team not set";
		}
		if(this.goals2 < 0) {
			return "Goals for away team not set";
		}
		if(!checkDuplicateIds() && !useNames) {
			return "Players have the same ID";
		}
		if(useNames && (this.homePlayerNames.length != this.awayPlayerNames.length)) {
			return "The teams have different number of player names";
		}
		if(!useNames && (this.homePlayers.length != this.awayPlayers.length)) {
			return "The teams have different number of player ids";
		}
		if(useNames && !checkDuplicateNames()) {
			return "Players have the duplicate or empty names";
		}
		if(this.key == null) {
			return "Empty key";
		}
		if(!Utils.validateMatchChecksum(this)) {
			return "Key is not correct";
		}
		
		return null;
		
	}
	
	private boolean checkDuplicateNames() {
		
		if(this.homePlayerNames == null || this.awayPlayerNames == null) {
			return false;
		}
		
		List<String> names = Arrays.asList(this.homePlayerNames);
		names.addAll(Arrays.asList(this.awayPlayerNames));
		
		if(new HashSet<String>(names).size() < names.size()) {
			return false;
		}
		else {
			return true;
		}
	}

	private boolean checkDuplicateIds() {
		
		List<Integer> ids = new ArrayList<Integer>();
		
		if(this.homePlayers != null) {
			for(int id : this.homePlayers) {
				ids.add(id);
			}
		}
		if(this.awayPlayers != null) {
			for(int id : this.awayPlayers) {
				ids.add(id);
			}
		}
		
		if(new HashSet<Integer>(ids).size() < ids.size()) {
			return false;
		}
		else {
			return true;
		}
		
	}
	
}
