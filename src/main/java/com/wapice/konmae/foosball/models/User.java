package com.wapice.konmae.foosball.models;

import java.text.Collator;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;

import com.google.gson.annotations.Expose;

public class User {
	
	
	/**
	 * Pelaajan tunnus
	 */
	@Expose
	public final int id;
	
	/**
	 * Pelaajan nimi
	 */
	@Expose
	public String name = "";
	
	/**
	 * Pelaajan ELO-pisteet.
	 * Lähtöarvo uudelle pelaajalle on 1000 ELO-pisteettä.
	 */
	@Expose
	public double points = 1000.0d;
	
	/**
	 * Pelaajan voittamien otteluiden lukumäärä
	 */
	@Expose
	public int wins = 0;
	
	/**
	 * Pelaajan häviämien otteluiden lukumäärä
	 */
	@Expose
	public int losses = 0;
	
	/**
	 * Pelaajan tasapelien lukumäärä
	 */
	@Expose
	public int draws = 0;
	
	/**
	 * Pelaajan pelaamien otteluiden lukumäärä
	 */
	@Expose
	public int played = 0;
	
	
	public User(String name, int id) {
		this.name = name;
		this.id = id;
	}
	
	/**
	 * Päivittää pelaajan pisteet ja tilastot.
	 * Voittojen, tappioiden tai tasapelien lukumäärää nostetaan ja ELO-pisteet päivitetään.
	 * Syötettävä {@link Match}-olio sisältää tiedon siitä, kuinka paljon kuinkin pelaajan pisteet ottelussa muuttuvat
	 * @param match Otteluolio, jonka perusteella käyttäjätieto päivitetään.
	 * @param i Pelaajan indeksi joukkuetaulukossa<br />(0 = joukkueensa ensimmäinen pelaaja, 1 = joukkueensa toinen pelaaja)
	 */
	public void updatePlayer(Match match, int i) {
		
		if(match == null) {
			return;
		}
		
		this.played++;
		
		boolean isInHomeTeam = Arrays.asList(match.homePlayers).contains(this); // Pelasiko pelaaja koti- vai vierasjoukkueessa?
		boolean isInAwayTeam = Arrays.asList(match.awayPlayers).contains(this);
		
		if(!isInHomeTeam && !isInAwayTeam) {
			return; // Pelaajan on oltava joko koti- tai vierasjoukkueessa!
		}
		
		double change = isInHomeTeam ? match.change1[i] : match.change2[i];
		
		if(match.result.isDraw()) {
			this.draws++;
		}
		
		else if((match.result.isHomeWinner() && isInHomeTeam) || (match.result.isAwayWinner() && isInAwayTeam)) {
			this.wins++;
		}
		
		else {
			this.losses++;
		}
		
		this.points += change; // ELO-pisteet on oltava laskettu jo Match-oliossa
			
	}
	
	@Override
	public boolean equals(Object otherUser) {
		
		try {
			
			return this.nameEquals(((User) otherUser).name) || this.id == ((User) otherUser).id;
			
		} catch(ClassCastException e) {
			return false;
		}
		
	}
	
	@Override
	public String toString() {
		return this.name;
	}
	
	/**
	 * Hakee Comparator-olion sopivaan vertailuun.
	 * @param value Sen attribuutin nimi, jonka perusteella vertaillaan
	 * @return Comparator-olio, joka vertailee annetun nimen perusteella
	 */
	public static Comparator<? super User> getComparator(String value) {
		
		if(value != null) {
			value = value.toLowerCase();
		}
		
		switch(value == null ? "" : value.toLowerCase()) {
			
		case "points":
			return new Comparator<User>() {
				@Override
				public int compare(User u1, User u2) {
					return (-1) * Double.compare(u1.points, u2.points);
				}
			};
		case "wins":
			return new Comparator<User>() {
				@Override
				public int compare(User u1, User u2) {
					return (-1) * Integer.compare(u1.wins, u2.wins);
				}
			};
		case "losses":
			return new Comparator<User>() {
				@Override
				public int compare(User u1, User u2) {
					return (-1) * Integer.compare(u1.losses, u2.losses);
				}
			};
		case "draws":
			return new Comparator<User>() {
				@Override
				public int compare(User u1, User u2) {
					return (-1) * Integer.compare(u1.draws, u2.draws);
				}
			};
		case "games":
			return new Comparator<User>() {
				@Override
				public int compare(User u1, User u2) {
					return (-1) * Integer.compare(u1.wins + u1.losses + u1.draws, u2.wins + u2.losses + u2.draws);
				}
			};
		case "name":
			return new Comparator<User>() {
				@Override
				public int compare(User u1, User u2) {
					return Collator.getInstance(new Locale("fi", "FI")).compare(u1.name, u2.name);
				}
			};
		default:
			return null;
		}
		
	
	}

	/**
	 * Suomenkielinen nimivertailu.
	 * @param name Vertailtava nimi suomeksi
	 * @return Tosi, jos tämän käyttäjän nimi on sama kuin annettu nimi 
	 */
	public boolean nameEquals(String name) {
		
		if(name == null) {
			return false;
		}
		
		name = name.toLowerCase(new Locale("fi", "FI"));
		String tmpName = this.name.toLowerCase(new Locale("fi", "FI"));
		
		return name.equals(tmpName);
		
		
	}
		
}
