package com.wapice.konmae.foosball.models;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.Expose;
import com.wapice.konmae.foosball.Database;
import com.wapice.konmae.foosball.Utils;
import com.wapice.konmae.foosball.models.json.MatchJson;
import com.wapice.konmae.foosball.utils.MatchResult;
import com.wapice.konmae.foosball.utils.RankingCalculator;

public class Match {
	
	/**
	 * Ottelun tunnus
	 */
	@Expose
	public int id = 0;
	
	/** 
	 * Kotijoukkueen maalimäärä
	 */
	@Expose
	public final int goals1;

	/** 
	 * Vierasjoukkueen maalimäärä
	 */
	@Expose
	public final int goals2;
	
	/**
	 * Ottelutulosolio
	 */
	public final MatchResult result;
	
	/** 
	 * Lista kotijoukkueen pelaajista
	 */
	public final User[] homePlayers;
	
	/** 
	 * Lista vierasjoukkueen pelaajista 
	 */
	public final User[] awayPlayers;
	
	/** 
	 * Taulukko kotijoukkueen pelaajien id:istä
	 */
	@Expose
	public final int[] team1Ids;

	/** 
	 * Taulukko kotijoukkueen pelaajien nimistä
	 */
	@Expose
	public final String[] team1Names;
	
	/** 
	 * Taulukko vierasjoukkueen pelaajien id:istä 
	 */
	@Expose
	public final int[] team2Ids;

	/** 
	 * Taulukko vierasjoukkueen pelaajien nimistä 
	 */
	@Expose
	public final String[] team2Names;
	
	/** 
	 * Taulukko kotijoukkueen pelaajien pistemuutoksista 
	 */
	@Expose
	public final double[] change1;

	/** 
	 * Taulukko vierasjoukkueen pelaajien pistemuutoksista 
	 */
	@Expose
	public final double[] change2;

	/** 
	 * Pelaajien lukumäärä per joukkue (1 tai 2)
	 */
	@Expose
	public final int playersPerTeam;
	
	/** 
	 * Ottelun ajankohta (dd.MM.yyyy HH:mm:ss) 
	 */
	@Expose
	public final String date;
	
	/**
	 * Käytetään vain kun tietoja ladataan databeissistä.
	 * Ei laske ranking-muutosta, koska se on jo databasen kentässä
	 * @param u1 Lista kotijoukkueen pelaajista
	 * @param u2 Lista vierasjoukkueen pelaajista
	 * @param homeGoals Kotijoukkueen maalit
	 * @param awayGoals Vierasjoukkueen maalit
	 * @param date Ottelun timestamp
	 * @param change1 Kotijoukkueen pelaajien pistemuutokset
	 * @param change2 Vierasjoukkueen pelaajien pistemuutokset
	 */
	public Match(User[] u1, User[] u2, int homeGoals, int awayGoals, String date, double[] change1, double[] change2) {
		
		if(u1.length != u2.length) {
			throw new IllegalArgumentException("Invalid match loaded from database: teams have different number of players");
		}
		
		this.playersPerTeam = u1.length;
		
		this.homePlayers = u1;
		this.awayPlayers = u2;
		this.goals1 = homeGoals;
		this.goals2 = awayGoals;
		this.result = new MatchResult(homeGoals, awayGoals);
		
		this.team1Ids = new int[this.playersPerTeam];
		this.team2Ids = new int[this.playersPerTeam];
		this.team1Names = new String[this.playersPerTeam];
		this.team2Names = new String[this.playersPerTeam];
		
		for(int i = 0; i < u1.length; i++) {
			
			this.team1Ids[i] = u1[i].id;
			this.team2Ids[i] = u2[i].id;
			this.team1Names[i] = u1[i].name;
			this.team2Names[i] = u2[i].name;
			
		}
		
		this.change1 = change1;
		this.change2 = change2;
		this.date = date;
		
	}
	
	/**
	 * Luo uuden 1vs1-ottelun ja laskee paljonko pisteet ottelutuloksella muuttuvat.
	 * {@link com.wapice.konmae.foosball.handlers.MatchHandler} hoitaa pelaajien pisteiden päivittämisen oikeiksi HTTP-kutsun jälkeen.
	 * @param u1 Kotijoukkueen 1. pelaaja
	 * @param u2 Vierasjoukkueen 1. pelaaja
	 * @param homeGoals Kotijoukkueen maalit
	 * @param awayGoals Vierasjoukkueen maalit
	 */
	public Match(User u1, User u2, int homeGoals, int awayGoals) {
		
		this.playersPerTeam = 1;
		this.homePlayers = new User[] {u1};
		this.awayPlayers = new User[] {u2};
		
		this.goals1 = homeGoals;
		this.goals2 = awayGoals;
		this.result = new MatchResult(homeGoals, awayGoals);
		
		this.team1Ids = new int[] {u1.id};
		this.team2Ids = new int[] {u2.id};
		this.team1Names = new String[] {u1.name};
		this.team2Names = new String[] {u2.name};
		
		this.date = Utils.getTimeStamp();
				
		// Laskee, paljonko pisteet muuttuisi. Otteluhandler kutsuu pelaajien rankingin päivitysmetodia
		RankingCalculator calculator = new RankingCalculator(u1.points, u2.points, new MatchResult(homeGoals, awayGoals));
		this.change1 = calculator.getRankingChange(true);
		this.change2 = calculator.getRankingChange(false);
		
	}
	
	/**
	 * Luo uuden 2vs2-otteln ja laskee, paljonko pisteet ottelutuloksen myötä muuttuisivat.
	 * {@link com.wapice.konmae.foosball.handlers.MatchHandler} hoitaa pelaajien pisteiden päivittämisen oikeiksi HTTP-kutsun jälkeen.
	 * @param u1 Kotijoukkueen 1. pelaaja
	 * @param u2 Kotijoukkueen 2. pelaaja
	 * @param u3 Vierasjoukkueen 1. pelaaja
	 * @param u4 Vierasjoukkueen 2. pelaaja
	 * @param homeGoals Kotijoukkueen maalit
	 * @param awayGoals Vierasjoukkueen maalit
	 */
	public Match(User u1, User u2, User u3, User u4, int homeGoals, int awayGoals) {
		
		this.playersPerTeam = 2;
		this.homePlayers = new User[] {u1, u2};
		this.awayPlayers = new User[] {u3, u4};
		
		this.goals1 = homeGoals;
		this.goals2 = awayGoals;
		this.result = new MatchResult(homeGoals, awayGoals);

		this.team1Ids = new int[] {u1.id, u2.id};
		this.team2Ids = new int[] {u3.id, u4.id};
		this.team1Names = new String[] {u1.name, u2.name};
		this.team2Names = new String[] {u3.name, u4.name};
		
		this.date = Utils.getTimeStamp();
				
		RankingCalculator calculator = new RankingCalculator(u1.points, u2.points, u3.points, u4.points, new MatchResult(homeGoals, awayGoals));
		this.change1 = calculator.getRankingChange(true);
		this.change2 = calculator.getRankingChange(false);
		
	}
	
	public static Match fromJsonObject(MatchJson json) throws IllegalArgumentException {
		
		String error = null;
		
		if(!json.checkKey()) {
			throw new IllegalArgumentException("Invalid key");
		}
		
		if((error = json.getError()) != null) {
			throw new JsonSyntaxException(error);
		}
		
		User[] u1 = null; // Kotijoukkue
		User[] u2 = null; // Vierasjoukkue
		
		if(json.useNames()) {
			
			String[] name1 = json.homePlayerNames;
			String[] name2 = json.awayPlayerNames;
			
			u1 = new User[name1.length];
			u2 = new User[name2.length]; // Pitäisi olla u1.length==u2.length...
			List<User> allUsers = new ArrayList<>(); // Duplikaattitarkistusta varten
			
			for(int i = 0; i < u1.length; i++) {
				
				String homeName;
				String awayName;
				
				try {
					homeName = URLDecoder.decode(name1[i], "UTF-8");
					awayName = URLDecoder.decode(name2[i], "UTF-8");
				} catch (UnsupportedEncodingException e) {
					homeName = name1[i];
					awayName = name2[i];
				}
				
				User homeUser = Database.get().getUser(homeName);
				User awayUser = Database.get().getUser(awayName);
				
				if(homeUser == null) {
					throw new JsonSyntaxException("Player #" + (i+1) + " in home team with name " + homeName + " doesn't exist");
				}
				if(awayUser == null) {
					throw new JsonSyntaxException("Player #" + (i+1) + " in away team with name " + awayName + " doesn't exist");					
				}
				
				if(allUsers.contains(homeUser)) {
					throw new JsonSyntaxException("Player with name " + homeName + " is already in a team");
				}
				allUsers.add(homeUser);
				
				if(allUsers.contains(awayUser)) {
					throw new JsonSyntaxException("Player with name " + awayName + " is already in a team");
				}
				
				allUsers.add(awayUser);
				
				u1[i] = homeUser;
				u2[i] = awayUser;
				
			}			
		}
		
		else {
			
			int[] id1 = json.homePlayers;
			int[] id2 = json.awayPlayers;
			u1 = new User[id1.length];
			u2 = new User[id2.length]; // Pitäisi olla u1.length==u2.length...
			List<User> allUsers = new ArrayList<>(); // Duplikaattitarkistusta varten
			
			for(int i = 0; i < id1.length; i++) {
				
				User homeUser = Database.get().getUser(id1[i]);
				User awayUser = Database.get().getUser(id2[i]);
				
				if(homeUser == null) {
					throw new JsonSyntaxException("Player #" + (i+1) + " in home team with id " + id1[i] + " doesn't exist");
				}
				if(awayUser == null) {
					throw new JsonSyntaxException("Player #" + (i+1) + " in away team with id " + id2[i] + " doesn't exist");					
				}
				
				if(allUsers.contains(homeUser)) {
					throw new JsonSyntaxException("Player with id " + id1[i] + " is already in a team");
				}
				allUsers.add(homeUser);
				
				if(allUsers.contains(awayUser)) {
					throw new JsonSyntaxException("Player with name " + id2[i] + " is already in a team");
				}
				
				allUsers.add(awayUser);
				
				u1[i] = homeUser;
				u2[i] = awayUser;
			}
		}
		
		if(u1.length == 1 && u2.length == 1) {
			return new Match(u1[0], u2[0], json.goals1, json.goals2);
		}
		else if(u1.length == 2 && u2.length == 2) {
			return new Match(u1[0], u1[1], u2[0], u2[1], json.goals1, json.goals2);
		}
		else {
			throw new JsonSyntaxException("Invalid number of players in teams: home team has " + u1.length + " players, away team " + u2.length + " players.");
		}
		
		
	}
	
	
	/**
	 * Päivittä pelaajien tiedot (pisteet, ottelumäärät jne.).
	 * Otteluhandler {@link com.wapice.konmae.foosball.handlers.MatchHandler} kutsuu tätä metodia, kun {@link Match}-olio on luotu
	 * ja ranking-pistemuutokset on laskettu.
	 */
	public void updatePlayers() {
		
		for(int i = 0; i < this.playersPerTeam; i++) {
			this.homePlayers[i].updatePlayer(this, i);
			this.awayPlayers[i].updatePlayer(this, i);
		}
	}
	
	
	
}
