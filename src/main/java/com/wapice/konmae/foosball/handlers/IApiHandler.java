package com.wapice.konmae.foosball.handlers;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.wapice.konmae.foosball.Main;
import com.wapice.konmae.foosball.Utils;

import xyz.konstamaenpanen.logger.LogManager;
import xyz.konstamaenpanen.logger.Logger;

public abstract  class IApiHandler implements HttpHandler {
	public static final Logger logger = LogManager.getLogger(Main.class.getName());

	protected String endpoint;
	protected List<String> methods;
	protected String request;
	protected String response;
	protected String error;
	protected int code;
	protected boolean isJson = true;
	protected String imageType = null;
	protected byte[] image = null;
	protected Map<String, String> queryParams;
	protected static final List<String> METHODS = Arrays.asList(new String[]{"GET", "PUT", "POST", "DELETE"});


	public IApiHandler(String endpoint, String...methods) throws NullPointerException, IllegalArgumentException {

		if(endpoint == null) {
			throw new NullPointerException("endpoint");
		}
		if(methods == null) {
			throw new NullPointerException("methods");
		}

		if(endpoint.length() == 0) {
			throw new IllegalArgumentException("endpoint length < 1");
		}
		if(methods.length == 0) {
			throw new IllegalArgumentException("no allowed methods");
		}

		this.methods = new ArrayList<>(methods.length);

		for(String method : methods) {

			method = method.toUpperCase();

			if(!METHODS.contains(method))
				throw new IllegalArgumentException("Unsupported method " + method);

			this.methods.add(method);

		}

		this.endpoint = endpoint;

	}

	@Override
	public final void handle(HttpExchange x) {

		this.code = 200;
		this.error = "Undefined error";
		this.response = "";
		
		String query = x.getRequestURI().getQuery();
		this.queryParams = query == null ? new LinkedHashMap<String, String>() : Utils.splitQuery(query);

		String path = x.getRequestURI().getPath();
		
		if(this.endpoint.endsWith("/")) {
			if(!path.startsWith(this.endpoint)) {
				Utils.writeErrorJSON("Invalid endpoint", x, 404);
				return;
			}
		}
		else if(!path.equals(this.endpoint)) {
			Utils.writeErrorJSON("Invalid endpoint", x, 404);
			return;
		}
		
		if(!this.methods.contains(x.getRequestMethod())) {
			Utils.writeErrorJSON("HTTP method " + x.getRequestMethod() + " not allowed to this endpoint", x, 405);
			return;
		}
		
		
		this.request = Utils.getRequestBody(x);
		boolean callBack = false;
		
		try {
			callBack = callback(x);
		} catch (Exception e) {
			
			this.code = 500;
			logger.log(e);
			
		}
		
		if(callBack) {
			if (this.imageType != null && this.image != null) {
				Utils.writeImage(this.image, x, this.imageType);
			} else {
				Utils.writeResponse(this.response == null ? "" : this.response, x, this.code, this.isJson);				
			}
		}
		else {
			logger.error(this.error);
			Utils.writeErrorJSON(this.error == null ? "Undefined error" : this.error, x, this.code);
		}

	}

	protected abstract boolean callback(HttpExchange x) throws Exception;


}
