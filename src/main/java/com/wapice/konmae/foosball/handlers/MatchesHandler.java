package com.wapice.konmae.foosball.handlers;

import com.sun.net.httpserver.HttpExchange;
import com.wapice.konmae.foosball.Database;
import com.wapice.konmae.foosball.Utils;

public class MatchesHandler extends IApiHandler {

	public MatchesHandler(String endpoint, String... methods) throws NullPointerException, IllegalArgumentException {
		super(endpoint, methods);
	}

	@Override
	public boolean callback(HttpExchange x) {
				
		this.code = 200;
		this.response = Utils.GSON.toJson(Utils.wrap("matches", Database.get().getMatches()));
		return true;
	}

}
