package com.wapice.konmae.foosball.handlers;

import com.google.gson.JsonSyntaxException;
import com.sun.net.httpserver.HttpExchange;
import com.wapice.konmae.foosball.Database;
import com.wapice.konmae.foosball.Utils;
import com.wapice.konmae.foosball.models.User;
import com.wapice.konmae.foosball.models.json.UserJson;

public class UserHandler extends IApiHandler {

	public UserHandler(String endpoint, String... methods) throws NullPointerException, IllegalArgumentException {
		super(endpoint, methods);
	}


	@Override
	protected boolean callback(HttpExchange x) {

		if("GET".equals(x.getRequestMethod())) {
			String[] splitted = x.getRequestURI().toString().split("/");
			int i = -1;
			String name = null;
			
			try {

				i = Integer.parseInt(splitted[splitted.length - 1]);

			} catch (Exception e) {
								
				if((name = this.queryParams.get("name")) == null) {

					this.code = 400;
					this.error = "No ID or name specified";
	
					return false;
				}
				else {
					
					User u = Database.get().getUser(name);
					
					if(u == null) {
						this.code = 404;
						this.error = "User with name \"" + name + "\" not found";
						return false;
					}
					else {
						this.code = 200;
						this.response = Utils.GSON.toJson(Utils.wrap("user", u));
						return true;
					}
					
				}

			}
						
			User user = Database.get().getUser(i);
			if (user == null) {

				this.code = 404;
				this.error = "User with id " + i + " not found";

				return false;

			} else {
				this.code = 200;
				this.response = Utils.GSON.toJson(Utils.wrap("user", user));
				return true;

			} 
			
		}
		else if("POST".equals(x.getRequestMethod())) {
			
			String[] splitted = x.getRequestURI().toString().split("/");
			
			try {
				
				if(!"add".equals(splitted[splitted.length - 1])) {
					throw new Exception("Endpoint is not \"user/add\" for POST");
				}
				
				try {
					
					UserJson u = Utils.GSON.fromJson(this.request, UserJson.class); 
					
					String correctKey = Utils.getSetting("user-key");
					
					if(correctKey == null) {
						correctKey = Utils.USER_KEY;
					}
					
					if(!correctKey.equals(u.key)) {
						
						this.code = 401;
						this.error = "Invalid key to add a new user";
						
						return false;
						
					}
					else {
						
						this.code = 201;
						
						try {
							
							int id = Database.get().putUser(u.name);
							this.response = Utils.GSON.toJson(Utils.wrap("user-id", id));
							return true;
							
						} catch(IllegalArgumentException e) {
							
							this.code = 400;
							this.error = e.getMessage();
							return false;
							
						}
						
					}
					
					
				} catch(JsonSyntaxException e) {
					
					this.code = 400;
					this.error = "Invalid JSON syntax: " + e.getMessage();

					return false;
				}
				
				
				
			} catch(Exception e) {
				
				logger.log(e);
				this.code = 405;
				this.error = "Invalid endpoint";
				return false;
				
			}
			
		}
		else {
			this.code = 500;
			this.error = "Unallowed method";
			return false;
		}
		
	}

}
