package com.wapice.konmae.foosball.handlers;

import com.sun.net.httpserver.HttpExchange;
import com.wapice.konmae.foosball.Database;
import com.wapice.konmae.foosball.Utils;

public class UsersHandler extends IApiHandler {

	
	public UsersHandler(String endpoint, String... methods) throws NullPointerException, IllegalArgumentException {
		super(endpoint, methods);
	}

	@Override
	protected boolean callback(HttpExchange x) {
		 
		this.code = 200;
		
		String order = this.queryParams.get("order");
		int limit = Utils.tryParseInt(this.queryParams.get("limit"), -1);
		boolean hasQuery = order != null || limit > 0;
		
		this.response = Utils.GSON.toJson(Utils.wrap("users", hasQuery ? Database.get().getUsers(order, limit) : Database.get().getUsers()));		
		return true;
	}

}
