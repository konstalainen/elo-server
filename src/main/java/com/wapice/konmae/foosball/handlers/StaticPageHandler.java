package com.wapice.konmae.foosball.handlers;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.sun.net.httpserver.HttpExchange;

public class StaticPageHandler extends IApiHandler {

	public StaticPageHandler(String endpoint, String... methods) throws NullPointerException, IllegalArgumentException {
		super(endpoint, methods);
	}

	@Override
	protected boolean callback(HttpExchange x) throws Exception {
		
		//logger.debug("Requesting a static web page: " + x.getRequestURI().toString());
		String[] splitted = x.getRequestURI().toString().split("/");
		
		
		String fileName = "";
		
		try {
			
			if (splitted.length == 0) {
				fileName = "index.html"; 
			} else {				
				fileName = splitted[splitted.length - 1];
			}
			
			this.code = 200;
			String mimeType = Files.probeContentType(new File("static/" + fileName).toPath());
			
			if (mimeType.startsWith("image/")) {
				this.imageType = mimeType;
				this.image = Files.readAllBytes(Paths.get("static/" + fileName));
			} else {
				this.imageType = null;
				this.image = null;
				this.response = new String(Files.readAllBytes(Paths.get("static/" + fileName)), StandardCharsets.UTF_8);
			}
			
			this.isJson = false;
			
			
			
		} catch(Exception e) {
			this.code = 404;
			this.isJson = true;
			this.error = "Page " + fileName + " not found: " + e.getMessage();
			logger.error(e.getClass().getCanonicalName() + ": " + e.getMessage());
			return false;
		}
		
		return true;
	}

}
