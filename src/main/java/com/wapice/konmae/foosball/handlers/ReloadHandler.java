package com.wapice.konmae.foosball.handlers;

import com.google.gson.JsonSyntaxException;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.wapice.konmae.foosball.Database;
import com.wapice.konmae.foosball.Utils;
import com.wapice.konmae.foosball.handlers.IApiHandler;
import com.wapice.konmae.foosball.models.json.KeyJson;

public class ReloadHandler extends IApiHandler implements HttpHandler {

	public ReloadHandler(String endpoint, String... methods) throws NullPointerException, IllegalArgumentException {
		super(endpoint, methods);
	}

	@Override
	public boolean callback(HttpExchange x) {
		
		String reloadKey = Utils.getSetting("reload-key") == null ? Utils.RELOAD_KEY : Utils.getSetting("reload-key");
		KeyJson json = null;
		
		try {
			
			json = Utils.GSON.fromJson(this.request, KeyJson.class);
			
		} catch(JsonSyntaxException e) {
			
			this.code = 400;
			this.error = "Invalid input JSON: " + e.getMessage();

			return false;
			
		}
		
		
		if(json == null || json.key == null) {
			
			this.code = 403;
			this.error = "Pass the correct reload key in a JSON object with a key called \"key\".";
			return false;
			
		}
		
		else if(json.key.equalsIgnoreCase(reloadKey)) {
			
			this.code = 200;
			Database.reload();
			this.response = Utils.GSON.toJson(Utils.wrap("loaded", "ok"));
			return true;
			
		}
		
		else {
			this.code = 401;
			this.error = "Invalid key.";
			return false;
		}
	
	}
}
