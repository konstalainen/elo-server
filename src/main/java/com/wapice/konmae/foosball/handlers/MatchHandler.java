package com.wapice.konmae.foosball.handlers;

import com.google.gson.JsonSyntaxException;
import com.sun.net.httpserver.HttpExchange;
import com.wapice.konmae.foosball.Database;
import com.wapice.konmae.foosball.Main;
import com.wapice.konmae.foosball.Utils;
import com.wapice.konmae.foosball.models.Match;
import com.wapice.konmae.foosball.models.json.MatchJson;

import xyz.konstamaenpanen.logger.LogManager;
import xyz.konstamaenpanen.logger.Logger;

public class MatchHandler extends IApiHandler {

	public static final Logger logger = LogManager.getLogger(Main.class.getName());

	public MatchHandler(String endpoint, String... methods) throws NullPointerException, IllegalArgumentException {
		super(endpoint, methods);
	}

	@Override
	public boolean callback(HttpExchange x) {
		
		Match match = null;
		logger.debug("Match json: " + this.request);
		
		try {
			
			if(this.request == null) {
				
				this.code = 400;
				this.error = "Request body missing";
				
				return false;
			}
			
			match = Match.fromJsonObject(Utils.GSON.fromJson(this.request, MatchJson.class));
			match.updatePlayers();
			
		} catch(IllegalArgumentException e) {
			
			this.code = 401;
			this.error = e.getMessage();
			
			return false;
			
		} catch(JsonSyntaxException e) {
			
			this.code = 400;
			this.error = "Invalid JSON syntax: " + e.getMessage();

			return false;
			
		} 

		
		Database.get().putMatch(match);
		this.code = 201; // Added		
		this.response = Utils.GSON.toJson(Utils.wrap("match", match));

		return true;
		
	}

}
