package com.wapice.konmae.foosball;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.sun.net.httpserver.HttpExchange;
import com.wapice.konmae.foosball.handlers.IApiHandler;

public class StartHandler extends IApiHandler {

	public StartHandler(String endpoint, String... methods) throws NullPointerException, IllegalArgumentException {
		super(endpoint, methods);
	}

	@Override
	protected boolean callback(HttpExchange x) throws Exception {
		this.code = 200;
		this.response = new String(Files.readAllBytes(Paths.get("documentation.html")), StandardCharsets.UTF_8);
		this.isJson = false;
		return true;
	}

}
