package com.wapice.konmae.foosball;

import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.sun.net.httpserver.HttpExchange;
import com.wapice.konmae.foosball.models.json.MatchJson;

import xyz.konstamaenpanen.logger.LogManager;
import xyz.konstamaenpanen.logger.Logger;

public class Utils {
	
	public static final Logger logger = LogManager.getLogger(Main.class.getName());
	public static final String RELOAD_KEY = "W1XHDy6JJKkQkol5cP8isIGI48Rwj2hXlndTlavN";
	public static final String USER_KEY = 	"6790776bb72de11d970848109b236cbdW21Ah721";
	private static final String MATCH_KEY = "ffEWk9KZOniNdO5v2JqcVdWyYOT9wRrqdvo61ehc";
	
	public static final Gson GSON = (new GsonBuilder()).excludeFieldsWithoutExposeAnnotation().create();
	public static final DateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	public static final TimeZone tz = TimeZone.getTimeZone("Europe/Helsinki");
	private static volatile MessageDigest md = null; 
	private static Map<String, String> settings = null;
	
	public static String getSetting(String key) {
		
		if(key == null) {
			return null;
		}
		
		if(settings == null) {
			try {
				
				String json = new String(Files.readAllBytes(Paths.get("settings.json")), StandardCharsets.UTF_8);
				settings = GSON.fromJson(json, new TypeToken<Map<String, String>>() {}.getType());
				logger.debug("Settings JSON loaded:");
				logger.debug("" + settings);
				
			} catch(JsonParseException | IOException e) {
				logger.log(e);
				settings = new HashMap<String, String>();
			}
		}
		
		return settings.get(key);
		
	}
	
	public static Date convert(String date) {
		
		format.setTimeZone(tz);
		
		if(date == null) {
			return null;
		}
		
		try {
			return format.parse(date);
		} catch (ParseException e) {
			logger.log(e);
			return null;
		}
		
	}
	
	public static String convert(Date date) {
		
		if(date == null) {
			date = new Date();
		}
		
		format.setTimeZone(tz);
		return format.format(date);
		
	}
	
	public static String getTimeStamp() {
		return Utils.convert(new Date());
	}
	
	public static boolean isNullOrEmpty(int[] i) {
		
		if(i == null) {
			return true;
		}
		else if(i.length == 0) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	public static <T> boolean isNullOrEmpty(T[] t) {
		
		if(t == null) {
			return true;
		}
		else if(t.length == 0) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	public static <T> Map<String, T> wrap(String key, T t) {
		
		Map<String, T> map = new HashMap<>();
		map.put(key, t);
		return map;
		
	}
	
	public static synchronized boolean validateMatchChecksum(MatchJson json) {
		
		if(md == null) {
			try {
				md = MessageDigest.getInstance("MD5");
			} catch(NoSuchAlgorithmException e) {
				logger.log(e);
				return false;
			}
		}
		
		if(json.key == null)
			return false;
		
		String checksum = getSetting("match-key") == null ? MATCH_KEY : getSetting("match-key");
		
//		String saltedString =
//				"" + json.player1 + json.player2 + (json.player1 + json.player2) + json.goals1 + json.goals2 + (json.goals1 + json.goals2);
//		
//		byte[] bytes = saltedString.getBytes();		
//		String checksum = DigestUtils.md5Hex(bytes);
		
		return checksum.equals(json.key);
		
	}
	
	public static Map<String, String> splitQuery(String queryString) {
		
	    Map<String, String> queryPairs = new LinkedHashMap<String, String>();
	    String[] pairs = queryString.split("&");
	    
	    for (String pair : pairs) {
	    	
	        int idx = pair.indexOf("=");
	        
	        try {
	        	
				queryPairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
				
			} catch (Exception e) {
				logger.log(e);
				continue;
			}
	    }
	    
	    return queryPairs;
	    
	}
	
	public static int tryParseInt(String str, int defaultValue) {
		try {
			return Integer.parseInt(str);
		} catch(Exception e) {
			return defaultValue;
		}
	}
	
	public static String getRequestBody(HttpExchange x) {
		
		String length = x.getRequestHeaders().getFirst("Content-length");
		
		if(length == null)
			return null;
		
		int len = -1;
		
		try {
			
			len = Integer.parseInt(length);
			
		} catch(NumberFormatException e) {
			logger.log(e);
		}
		
		try(InputStream reader = x.getRequestBody()) {
			
			if(len <= 0)
				return null;
			
			byte[] bytes = new byte[len];
			reader.read(bytes, 0, len);
			String s = new String(bytes, StandardCharsets.UTF_8);
			return s;
			
		} catch (IOException e) {
			logger.log(e);
			return null;
		}
		
	}
	
	public static void writeErrorJSON(String error, HttpExchange x, int code) {
		
		String json = GSON.toJson(Utils.wrap("error", error));
		writeResponse(json, x, code);
		
	}
	
	public static void writeResponse(String response, HttpExchange x, int code, boolean json) {
		
		byte[] b = response.getBytes(StandardCharsets.UTF_8);
		
		try {
			
			x.getResponseHeaders().add("Content-type", "" + (json ? "application/json" : "text/html") + "; charset=utf-8");
			x.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
			x.sendResponseHeaders(code, b.length);
			
			try(OutputStream out = x.getResponseBody()) {
				out.write(b);
			}
			
		} catch(IOException e) {
			logger.log(e);
		}
		
	}
	
	public static void writeImage(byte[] image, HttpExchange x, String imageType) {
		
		try {
			
			x.getResponseHeaders().add("Content-type", imageType);
			x.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
			x.sendResponseHeaders(200, image.length);
			
			try(OutputStream out = x.getResponseBody()) {
				out.write(image);
			}
			
		} catch(IOException e) {
			logger.log(e);
		}
		
	}
	
	public static void writeResponse(String response, HttpExchange x, int code) {
		Utils.writeResponse(response, x, code, true);
	}
}
