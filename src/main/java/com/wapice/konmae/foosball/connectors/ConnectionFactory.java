package com.wapice.konmae.foosball.connectors;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import com.wapice.konmae.foosball.Main;
import com.wapice.konmae.foosball.Utils;

import xyz.konstamaenpanen.logger.LogManager;
import xyz.konstamaenpanen.logger.Logger;

public class ConnectionFactory {
	
	public static final Logger logger = LogManager.getLogger(Main.class.getName());
	private static final List<String> allowedProperties = Arrays.asList("address", "schema", "user", "password", "timezone");
	
	public static IDatabaseConnector create(String identifier) throws Exception {
		
		if("".equals(identifier) || identifier == null) {
			logger.log("Using Mock Database");
			identifier = "MockDatabase";
		}
		
		logger.log("Trying to register database connector \"" + identifier + "\"");
		
		IDatabaseConnector conn = null;
		conn = (IDatabaseConnector) Class.forName("com.wapice.konmae.foosball.connectors." + identifier).newInstance();
		
		
		Properties prop = new Properties();
		String prefix = identifier.toLowerCase() + "-";
		
		for(String property : allowedProperties) {
			
			String value = Utils.getSetting(prefix + property);
			
			if(value != null) {
				prop.setProperty(property, value);
			}
			
		}
		
		logger.debug("Connetion properties set: " + prop);
		conn.setProperties(prop);
		conn.connect();
		conn.test();
		return conn;
	}
	
}
