package com.wapice.konmae.foosball.connectors;

import java.util.Map;
import java.util.Properties;

import com.wapice.konmae.foosball.models.Match;
import com.wapice.konmae.foosball.models.User;

public interface IDatabaseConnector {
	
	public void setProperties(Properties properties);
	public void connect() throws Exception;
	public Map<Integer, User> getUsers();
	public Map<Integer, Match> getMatches(Map<Integer, User> users);
	public int putUser(String name);
	public int putMatch(Match m);
	public boolean updateUser(User u);
	void test() throws Exception;
}
