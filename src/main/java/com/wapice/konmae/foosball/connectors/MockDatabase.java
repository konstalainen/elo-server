package com.wapice.konmae.foosball.connectors;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.wapice.konmae.foosball.Main;
import com.wapice.konmae.foosball.models.Match;
import com.wapice.konmae.foosball.models.User;

import xyz.konstamaenpanen.logger.LogManager;
import xyz.konstamaenpanen.logger.Logger;

public class MockDatabase implements IDatabaseConnector {
	
	public static final Logger logger = LogManager.getLogger(Main.class.getName());
	private static final Map<Integer, Match> emptyMatches = new HashMap<>();
	//private static final Map<Integer, User> emptyUsers = new HashMap<>();
	private int matchId = 0;
	
	@Override
	public void setProperties(Properties properties) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connect() throws Exception {
		
		logger.warn("No database connected. Your information will disappear when server disconnects.");

	}

	@Override
	public Map<Integer, User> getUsers() {
		return getMockUsers();
	}

	@Override
	public Map<Integer, Match> getMatches(Map<Integer, User> users) {
		return emptyMatches;
	}

	@Override
	public int putUser(String name) {
		return -1;
	}

	@Override
	public int putMatch(Match m) {
		matchId++;
		return matchId;
		
	}

	@Override
	public boolean updateUser(User u) {
		return false;
	}
	
	private static Map<Integer, User> getMockUsers() {
		
		String[] mockNames = {"Konsta Mäenpänen", "Kim Schmiedehausen", "Tomi Lähteenmäki", "Pasi Tuominen", "Marek Kwitek"};
		
		Map<Integer, User> mock = new HashMap<>();
		
		int i = 0;
		for(String name : mockNames) {
			i++;
			mock.put(i, new User(name, i));
		}
		
		return mock;
		
	}

	@Override
	public void test() throws Exception {
		
		
	}

}
