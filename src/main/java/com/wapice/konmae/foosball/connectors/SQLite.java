package com.wapice.konmae.foosball.connectors;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.wapice.konmae.foosball.Main;
import com.wapice.konmae.foosball.Utils;
import com.wapice.konmae.foosball.models.Match;
import com.wapice.konmae.foosball.models.User;
import com.wapice.konmae.foosball.utils.StatementCreator;

import xyz.konstamaenpanen.logger.LogManager;
import xyz.konstamaenpanen.logger.Logger;

public class SQLite implements IDatabaseConnector {
	
	public static final Logger logger = LogManager.getLogger(Main.class.getName());
	private Properties properties = null;
	private Connection conn = null;

	@Override
	public void setProperties(Properties properties) {
		
		this.properties = new Properties();
		this.properties.setProperty("address", properties.getProperty("address", "localhost:3306"));
		this.properties.setProperty("schema", properties.getProperty("schema", "foosball.db"));
		
	}
	
	@Override
	public void connect() throws Exception {
		
		logger.log("SQLite connecting");
		Class.forName("org.sqlite.JDBC");
		this.conn = DriverManager.getConnection("jdbc:sqlite:" + properties.getProperty("schema"));
		logger.log("SQLite connected successfully");
		
	}

	@Override
	public Map<Integer, User> getUsers() {
		
		Map<Integer, User> data = new HashMap<>();
		
		try {
		
			Statement stm = this.conn.createStatement();
			ResultSet result = stm.executeQuery("SELECT * FROM users");

			while(result.next()) {
				
				int id = result.getInt("id");
				String name = result.getString("name");
				
				int wins = result.getInt("wins");
				int draws = result.getInt("draws");
				int losses = result.getInt("losses");
				int played = wins + draws + losses;
				double points = result.getDouble("points");
				
				User u = new User(name, id);
				u.wins = wins;
				u.draws = draws;
				u.losses = losses;
				u.played = played;
				u.points = points;
				
				data.put(id, u);
				
			}
		
		} catch (SQLException e) {
			logger.log(e);
		}
		
		return data;
	}

	@Override
	public Map<Integer, Match> getMatches(Map<Integer, User> users) {
		
		Map<Integer, Match> data = new HashMap<>();
		
		try {
		
			Statement stm = this.conn.createStatement();
			ResultSet result = stm.executeQuery("SELECT * FROM matches");

			while(result.next()) {
				
				int id = result.getInt("id");
				int playersPerTeam = 1;
				int player1id = result.getInt("player1"); // team1, pelaaja 1
				int player2id = result.getInt("player2"); // team1, pelaaja2: -1 jos 1vs1
				int player3id = result.getInt("player3");// team2, pelaaja 1
				int player4id = result.getInt("player4"); // team2, pelaaja2 : -1 jos 1vs1
				
				double player1_change = result.getDouble("change1");
				double player2_change = result.getDouble("change2"); // Ignoorataan jos 1vs1
				double player3_change = result.getDouble("change3");
				double player4_change = result.getDouble("change4"); // Ignoorataan jos 1vs1
				
				if (player2id >= 0 && player4id >= 0) {
					playersPerTeam = 2;
				}
				
				User[] team1 = null;
				User[] team2 = null;
				double[] changesTeam1 = null;
				double[] changesTeam2 = null;
				
				if(playersPerTeam == 2) {
					
					team1 = new User[] { users.get(player1id), users.get(player2id) };
					team2 = new User[] { users.get(player3id), users.get(player4id) };
					changesTeam1 = new double[] { player1_change, player2_change };
					changesTeam2 = new double[] { player3_change, player4_change };
					
				} else {
					team1 = new User[] { users.get(player1id) };
					team2 = new User[] { users.get(player3id) };
					changesTeam1 = new double[] { player1_change };
					changesTeam2 = new double[] { player3_change };
				}
							
				if (team1[0] == null) {
					logger.error("Home team player 1 with id " + player1id + " not found");
				} else if (team2[0] == null) {
					logger.error("Away team player 1 with id " + player3id + " not found");
				} else if (playersPerTeam == 2 && team1[1] == null) {
					logger.error("Home team player 2 with id " + player2id + " not found");
				} else if (playersPerTeam == 2 && team2[1] == null) {
					logger.error("Away team player 2 with id " + player4id + " not found");
				} else {
			
					
					int goals1 = result.getInt("goals1");
					int goals2 = result.getInt("goals2");
					
					String timestamp = result.getString("date");
					
					Match m = new Match(team1, team2, goals1, goals2, timestamp, changesTeam1, changesTeam2);
					m.id = id;
					data.put(id, m);
				}
			}
		
		} catch (SQLException e) {
			logger.log(e);
		}
		
		return data;
	}

	@Override
	public int putUser(String name) {
		
		try {
			PreparedStatement stmt = this.conn.prepareStatement("INSERT INTO users (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, name); // Indeksointi alkaa 1:stä!
			int rows = stmt.executeUpdate();
			
			if(rows == 0) {
				return -1;
			}
			
			ResultSet result = stmt.getGeneratedKeys();
			
			if(result.next())
				return result.getInt(1);
			
			return -1;
			
		} catch (Exception e) {

			logger.log(e);
			return -1;
		}
	}

	@Override
	public int putMatch(Match m) {
		
		int matchId = -1;
		
		try {
			
			PreparedStatement stmt = new StatementCreator(this.conn).createPutMatch(m, String.class).getStatement();					
			int rows = stmt.executeUpdate();
			
			if(rows == 0) {
				return -1;
			}
			
			ResultSet result = stmt.getGeneratedKeys();
			matchId = -1;
			
			if(result.next())
				matchId = result.getInt(1);
			
			else
				return -1;
			
		} catch (SQLException|IllegalArgumentException e) {
			
			logger.log(e);
			return -1;
			
		}
		
		// Pelaaja-olioiden ottelumäärät ja pisteet ovat päivitetty ottelun luonnin yhteydessä -> päivitetään tietokantaan
		for(int i = 0; i < m.playersPerTeam; i++) {
			this.updateUser(m.homePlayers[i]);
			this.updateUser(m.awayPlayers[i]);
		}
		
		return matchId;
	}

	@Override
	public boolean updateUser(User u) {
		
		try {
			
			new StatementCreator(this.conn).createUpdateUser(u).getStatement().executeUpdate();
			return true;
			
		} catch(SQLException e) {
			logger.log(e);
			return false;
		}
		
	}

	@Override
	public void test() throws Exception {
		
		logger.debug("Running test query \"SELECT * FROM matches\"");
		logger.debug("Running test query \"SELECT * FROM users\"");
		this.conn.createStatement().executeQuery("SELECT * FROM matches");
		this.conn.createStatement().executeQuery("SELECT * FROM users");
		logger.log("SQLite tests passed!");
		
	}

}
