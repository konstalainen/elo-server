package com.wapice.konmae.foosball;
import java.io.IOException;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.*;
import com.wapice.konmae.foosball.handlers.*;

import xyz.konstamaenpanen.logger.LogManager;
import xyz.konstamaenpanen.logger.Logger;

public class EloServer {
	
	public static final Logger logger = LogManager.getLogger(Main.class.getName());
	private final HttpServer server;
	
	public EloServer(String ip, int port, boolean writeMode) throws IOException {
		
		InetSocketAddress address = new InetSocketAddress(ip, port);
		this.server = HttpServer.create(address, 0);
				
		logger.debug("Elo server created to " + address);
				
		this.createPublicContexts();
		
		if(writeMode) {
			
			if("0.0.0.0".equals(ip)) {
				logger.warn("Host address is \"0.0.0.0\" and read-only mode is not applied, this may be risky");
			}
			
			logger.log("writeMode == true, creating the POST endpoints too");
			this.createPrivateContexts();
		}
		else {
			logger.log("Write mode == false, read-only mode applied.");
		}
		
		logger.log("Elo server created");
		
	}
	
	public void start() {
		
		logger.log("Server started");
		this.server.start();
		
	}

	private void createPrivateContexts() {
		
		this.server.createContext("/", new StaticPageHandler("/", "GET"));
		this.server.createContext("/api/user/add", new UserHandler("/api/user/add", "POST"));
		this.server.createContext("/api/match/add", new MatchHandler("/api/match/add", "POST"));
		this.server.createContext("/api/reload", new ReloadHandler("/api/reload", "POST"));
		
	}
	
	private void createPublicContexts() {
		this.server.createContext("/api/users", new UsersHandler("/api/users", "GET"));
		this.server.createContext("/api/matches", new MatchesHandler("/api/matches", "GET"));
		this.server.createContext("/api/user/", new UserHandler("/api/user/", "GET"));

	}
	
	public void stop() {
		
		this.server.stop(0);
		
	}
	
}
