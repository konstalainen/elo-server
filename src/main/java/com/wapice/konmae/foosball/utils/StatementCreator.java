package com.wapice.konmae.foosball.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;

import com.wapice.konmae.foosball.Utils;
import com.wapice.konmae.foosball.models.Match;
import com.wapice.konmae.foosball.models.User;

public class StatementCreator {
	
	private PreparedStatement statement;
	private Connection connection;
	
	
	public StatementCreator(Connection connection) {
		this.connection = connection;
		this.statement = null;
	}
	
	public StatementCreator createPutMatch(Match m, Class timestampClass) throws SQLException, IllegalArgumentException {
				
		PreparedStatement stmt = this.connection.prepareStatement(
				"INSERT INTO matches (player1, player2, player3, player4, goals1, goals2, date, change1, change2, change3, change4) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", 
				Statement.RETURN_GENERATED_KEYS);
		
		stmt.setInt(1, m.homePlayers[0].id);
		stmt.setInt(2, m.playersPerTeam == 2 ? m.homePlayers[1].id : -1);
		stmt.setInt(3, m.awayPlayers[0].id);
		stmt.setInt(4, m.playersPerTeam == 2 ? m.awayPlayers[1].id : -1);
		stmt.setInt(5, m.goals1);
		stmt.setInt(6, m.goals2);
		
		Date date = Utils.convert(m.date);
		
		if(date == null) {
			throw new IllegalArgumentException("Date is empty");
		}
		
		if (timestampClass == String.class) {
			stmt.setString(7, Utils.getTimeStamp());
		} else {
			stmt.setTimestamp(7, new Timestamp(date.getTime()));
		}
		
		stmt.setDouble(8, m.change1[0]);
		stmt.setDouble(9, m.playersPerTeam == 2 ? m.change1[1] : 0.0d);
		stmt.setDouble(10, m.change2[0]);
		stmt.setDouble(11, m.playersPerTeam == 2 ? m.change2[1] : 0.0d);
		
		this.statement = stmt;		
		return this;
	}
	
	public StatementCreator createUpdateUser(User u) throws SQLException {
		
		PreparedStatement stmt = this.connection.prepareStatement("UPDATE users SET points = ?, wins = ?, draws = ?, losses = ? WHERE id = ?");
		stmt.setDouble(1, u.points);
		stmt.setInt(2, u.wins);
		stmt.setInt(3, u.draws);
		stmt.setInt(4, u.losses);
		stmt.setInt(5, u.id);
		
		this.statement = stmt;
		return this;
		
	}
	
	public PreparedStatement getStatement() {
		return this.statement;
	}
}
