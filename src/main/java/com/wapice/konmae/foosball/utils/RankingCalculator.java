package com.wapice.konmae.foosball.utils;

public class RankingCalculator {
	
	
	private static final double ELO_DIVIDOR = 400.0d;
	private static final double ELO_COEFF = 30.0d;
	
	private final int playersPerTeam;
	
	/** Kotijoukkueen 1. pelaajan pisteet */
	private final double homePoints_1;
	/** Kotijoukkueen 2. pelaajan pisteet */
	private final double homePoints_2;
	
	/** Vierasjoukkueen 1. pelaajan pisteet */
	private final double awayPoints_1;
	/** Vierasjoukkueen 2. pelaajan pisteet */
	private final double awayPoints_2;
	
	private final MatchResult result;
	
	/**
	 * Tekee olion, joka laskee ELO-ratingmuutoksen kaksinpelissä (1vs1)
	 * @param points1 kotijoukkueen pelaajan pisteet
	 * @param points2 vierasjoukkueen pelaajan pisteet
	 * @param result ottelutuloksen sisältävä olio
	 */
	public RankingCalculator(double points1, double points2, MatchResult result) {
		
		this.playersPerTeam = 1;
		
		this.homePoints_1 = points1;
		this.homePoints_2 = 0;
		this.awayPoints_1 = points2;
		this.awayPoints_2 = 0;
		
		this.result = result;
		
	}
	
	/**
	 * Tekee olion, joka laskee ELO-ratingmuutoksen nelinpelissä (2vs2)
	 * @param points1 kotijoukkueen 1. pelaajan pisteet
	 * @param points2 kotijoukkueen 2. pelaajan pisteet
	 * @param points3 vierasjoukkueen 1. pelaajan pisteet
	 * @param points4 vierasjoukkueen 2. pelaajan pisteet
	 * @param result ottelutuloksen sisältävä olio
	 */
	public RankingCalculator(double points1, double points2, double points3, double points4, MatchResult result) {
		
		this.playersPerTeam = 2;
		
		this.homePoints_1 = points1;
		this.homePoints_2 = points2;
		this.awayPoints_1 = points3;
		this.awayPoints_2 = points4;
		
		this.result = result;
	}
	
	/**
	 * Laskee ja palauttaa ottelutulokseen ja pisteisiin perustuvan ranking-muutoksen joukkueen molemmille pelaajille.
	 * Palautettavassa taulukossa on vain yksi elementti, jos kyseessä oli 1vs1-ottelu. 2vs2-pelissä on kaksi elementtiä.
	 * @param homeTeam true, jos halutaan kotijoukkueen tulokset. false jos vierasjoukkueen.
	 * @return double-taulukko jossa joukkueen 1. pelaajan ja 2. pelaajan pistemuutos.
	 */
	public double[] getRankingChange(boolean homeTeam) {
		
		double[] changes = new double[this.playersPerTeam];
		double teamPoints1 = homeTeam ? this.homePoints_1 : this.awayPoints_1;
		double teamPoints2 = homeTeam ? this.homePoints_2 : this.awayPoints_2;
		
		double oppositionPoints1 = homeTeam ? this.awayPoints_1 : this.homePoints_1;
		double oppositionPoints2 = homeTeam ? this.awayPoints_2 : this.homePoints_2;
		
		double actual = ((this.result.isHomeWinner() && homeTeam) || (this.result.isAwayWinner() && !homeTeam)) ? 1.0 : (this.result.isDraw() ? 0.5 : 0.0);
		
		if(this.playersPerTeam == 2) {
			
			double changePlayer1 = (this.calculateELO(teamPoints1, oppositionPoints1, actual) + this.calculateELO(teamPoints1, oppositionPoints2, actual)) / 2.0;
			double changePlayer2 = (this.calculateELO(teamPoints2, oppositionPoints1, actual) + this.calculateELO(teamPoints2, oppositionPoints2, actual)) / 2.0;
			
			changes[0] = changePlayer1;
			changes[1] = changePlayer2;
			
		}
		else {
			
			changes[0] = this.calculateELO(teamPoints1, oppositionPoints1, actual);
		}
		
		return changes;
		
	}
	
	
	
	/**
	 * Laskee ranking-muutoksen elo-järjestelmässä
	 * @param ra pelaajan ranking-pisteet
	 * @param rb vastustajan ranking-pisteet
	 * @param actual ottelun "varsinainen" tulos: 1.0 jos pelaaja voitti, 0.5 jos tasan ja 0.0 jos tappio
	 * @return ranking-pistemuutos
	 */
	private double calculateELO(double ra, double rb, double actual) {
		
		double expected = 1.0 / (1.0 + Math.pow(10.0, (rb-ra) / ELO_DIVIDOR));		
		double change = ELO_COEFF*(actual - expected);
		
		return change;
		
	}
	
}
