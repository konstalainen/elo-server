package com.wapice.konmae.foosball.utils;

public class MatchResult {
	
	private final int goals1;
	private final int goals2;
	
	/**
	 * Luo uusi ottelutulosolio
	 * @param goals1 Kotijoukkueen maalimäärä
	 * @param goals2 Vierasjoukkueen maalimäärä
	 */
	public MatchResult(int goals1, int goals2) {
		
		this.goals1 = goals1;
		this.goals2 = goals2;
		
	}
	
	/**
	 * Hakee ottelun tuloksen maalien perusteella
	 * @return 1, jos kotijoukkue voitti<br/>0, jos tasapeli<br/>-1, jos vierasjoukkue voitti
	 */
	public int getResult() {
		
		if(this.goals1 > this.goals2) {
			return 1;
		}
		else if (this.goals1 < this.goals2) {
			return -1;
		}
		else {
			return 0;
		}
		
	}
	
	/**
	 * Oliko tasapeli
	 * @return true, jos tasapeli
	 */
	public boolean isDraw() {
		return this.getResult() == 0;
	}
	
	/**
	 * Voittiko kotijoukkue
	 * @return true, jos kotijoukkue voitti
	 */
	public boolean isHomeWinner() {
		return this.getResult() > 0;
	}
	
	/**
	 * Voittiko vierasjoukkue
	 * @return true, jos vierasjoukkue voitti
	 */
	public boolean isAwayWinner() {
		return this.getResult() < 0;
	}
	
	

}
