package com.wapice.konmae.foosball;

import java.io.File;
import java.io.IOException;
import xyz.konstamaenpanen.logger.*;

public class Main {
	
	public static final Logger logger = LogManager.getLogger(Main.class.getName());
	
	public static void main(String...args) throws IOException, InterruptedException {
		
		int port = 5000;
		String ip = "localhost";
		
		try {
			
			ip = args[0];
		} catch(ArrayIndexOutOfBoundsException e) {
			
			ip = Utils.getSetting("host-address");
						
			if(ip == null) {
				logger.warn("Host address not defined in settings or args, using localhost");
				ip = "localhost";
			}
			
		}
		
		try {
			
			try {
				
				port = Integer.parseInt(args[1]);
				logger.debug("Port " + port + " was defined in args");
				
			} catch(Exception e) {
				
				port = Integer.parseInt(Utils.getSetting("host-port"));
				
			}
			
		} catch(Exception e) {
			
			logger.warn("No port defined, use default port " + port);
		}
		
		logger.debug("Host:port = " + ip + ":" + port);
		
		logger.log("Trying to find documentation.html from root");
		File f = new File("documentation.html");
		
		if(f.isFile() && f.exists()) {
			logger.log("Documentation file found at " + f.getAbsolutePath());
		}
		else {
			logger.error("Failed to find documentation file at " + f.getAbsolutePath());
			return;
		}
		
		try {
			
			logger.log("Testing database configuration...");
			Database.get();
			
		} catch(Exception e) {
			
			logger.log(e);
			return;
			
		}
		
		boolean writeMode = "true".equals(Utils.getSetting("writing-enabled"));		
		EloServer eloServer = new EloServer(ip, port, writeMode);
		eloServer.start();
		
		
		while(true) {
			
			try {
				
				Thread.sleep(1000);
				
			} catch(InterruptedException e) {
				logger.log(e);
				break;
			}
			
		}
		
		eloServer.stop();
		
		logger.log("Closing");
		
	}
	
}
