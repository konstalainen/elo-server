package xyz.konstamaenpanen.logger;

import java.util.Map;
import java.nio.file.Paths;
import java.util.HashMap;

/**
 * Use this class to create an instance of {@link Logger}.
 * <p>
 * You can create multiple logger instances if you want. But why?
 * </p>
 * @author Konsta Mäenpänen
 *
 */
public final class LogManager {
	
	private static Map<String, Logger> loggers = new HashMap<>();
	
	/**
	 * Gets the logger. Creates a new if not found with the given identifier.
	 * @param identifier identifier of the logger, usually the name of the class
	 * @return the logger
	 * @throws IllegalArgumentException if identifying string is null or empty
	 */
	public static synchronized Logger getLogger(String identifier) throws IllegalArgumentException {
		
		if(identifier == null || "".equals(identifier)) {
			throw new IllegalArgumentException("identifier is empty");
		}
		
		Logger l =  loggers.get(identifier);
		
		if(l == null) {
			
			loggers.put(identifier, new Logger(Paths.get("").toAbsolutePath().toString(), false, Severity.DEBUG));
			l = loggers.get(identifier);
		}
		
		return l;
		
	}
	
	
}
