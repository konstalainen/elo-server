package xyz.konstamaenpanen.logger;

/**
 * Defines the severity of a logged message
 * 
 * @author Konsta Mäenpänen
 */
public enum Severity {
	/**
	 * Used for debugging
	 */
	DEBUG, 
	/**
	 * Used for informatic messages
	 */
	INFO, 
	/**
	 * Used to indicate warnings
	 */
	WARNING, 
	/**
	 * Used to indicate errors and exceptions
	 */
	ERROR

}
