package xyz.konstamaenpanen.logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;

/**
 * Logger class.
 * 
 * <p>
 * Create a new logger with  {@link LogManager#getLogger(String)}.
 * Logger will log the messages to the log file and also will print them to the standard output stream. Errors will be written to standard error stream (stderr).
 * Also a nice timestamp will be printed at the beginning of the log message.
 * The exceptions can be logged simply by passing the exception to {@link Logger#log(Throwable)}, also the stack trace will be logged to help debugging.
 * </p>
 * @author Konsta Mäenpänen
 *
 */
public final class Logger {
	
	private final String logFolder;
	private final boolean silent;
	private final Severity minLevel;
	
	/**
	 * Instantiate a new Logger.
	 * @param root path where the "/logs" folder will be created 
	 * @param silent if true, no messages will be printed to stdout/stderr (only to file)
	 * @param minLevel minimum severity for logging a message (to a file)
	 */
	Logger(String root, boolean silent, Severity minLevel) {
		
		this.silent = silent;
		this.minLevel = minLevel;
		
		if(root == null) {
			root = Paths.get("").toAbsolutePath().toString();
		}
		
		String logFolder = root + File.separatorChar + "logs";
		System.out.println(String.format("[%s]\tINFO\tLogs will be in folder %s", Utils.getTimeStamp(), logFolder));
		
		try {
			File folder = new File(logFolder);
			
			if(!folder.isDirectory()) {
				folder.mkdirs();
				System.out.println(String.format("[%s]\tWARNING\tFolder %s not found, created", Utils.getTimeStamp(), logFolder));
			}

		} catch(Exception e) {
			System.err.println(String.format("[%s]\tERROR\tFailed to create folder %s. Using the default folder.", Utils.getTimeStamp(), logFolder));
			System.err.println(String.format("[%s]\tERROR\t%s: %s", Utils.getTimeStamp(), e.getClass().getSimpleName(), e.getMessage()));
			logFolder = "";
		}
		
		this.logFolder = logFolder;
		
	}
	
	/**
	 * Logs a throwable (i.e. an exception). 
	 * <p>Both the message and the stack trace will be logged. Prints the exception stack trace to the stderr stream if writing to the log file fails.</p>
	 * @param e the throwable to be written
	 */
	public void log(Throwable e) {
		
		try(PrintWriter pw = new PrintWriter(new FileWriter(getFile(), true), true)) {
			
			String prefix = "[" + Utils.getTimeStamp() + "]\t" + Severity.ERROR;
			
			if(!this.silent)
				System.err.println(prefix + "\t" + e.getClass().getSimpleName() + ": \"" + e.getMessage() + "\"");
			
			pw.println(prefix + "\tException caught");
			e.printStackTrace(pw);
			pw.flush();
			
		} catch (IOException ex) {
			
			e.printStackTrace();
			ex.printStackTrace();
			
		} 
		
	}
	
	/**
	 * Logs a message with the given severity level.
	 * @param level the severity level of the log message
	 * @param message content
	 */
	public void log(Severity level, String message) {
		
		message = "[" + Utils.getTimeStamp() + "]\t" + level + "\t" + message;
		
		if(!this.silent) {
			if(level == Severity.ERROR)
				System.err.println(message);
			else
				System.out.println(message);
		}
		
		if(level.compareTo(this.minLevel) < 0)
			return;
		
		try(PrintWriter pw = new PrintWriter(new FileWriter(getFile(), true), true)) {
			
			pw.println(message);
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} 
		
	}
	
	/**
	 * Log an informatic message
	 * <p>Same as calling log(Severity.INFO, message)</p>
	 * @param message content
	 */
	public void log(String message) {
		this.log(Severity.INFO, message);
	}
	
	/**
	 * Log a debug message
	 * <p>Same as calling log(Severity.DEBUG, message)</p>
	 * @param message content
	 */
	public void debug(String message) {
		this.log(Severity.DEBUG, message);
	}
	
	/**
	 * Log a warning message
	 * <p>Same as calling log(Severity.WARNING, message)</p>
	 * @param message content
	 */
	public void warn(String message) {
		this.log(Severity.WARNING, message);
	}
	
	/**
	 * Log an error message
	 * <p>Same as calling log(Severity.ERROR, message)</p>
	 * @param message content
	 */
	public void error(String message) {
		this.log(Severity.ERROR, message);
	}

	/**
	 * Get the log file where to write.
	 * <p>The name of the file will be in format <i>yyyyMMDD.log</i> to separate the logging messages from different dates.
	 * Creates a new file if not found.</p>
	 * @return handle to the file
	 * @throws IOException if fails to create the new file
	 * @see Utils#getTimeStamp()
	 */
	private File getFile() throws IOException {
		
		String fileName = Utils.getFileDatePrefix() + ".log";
		File f = new File(logFolder + File.separatorChar + fileName);
		
		if(!f.exists()) {
			
			if(!this.silent)
				System.out.println("[" + Utils.getTimeStamp() + "]\tINFO\tFile " + f.getAbsolutePath() + " doesn't exist, creating...");
			
			f.createNewFile();
		}
		
		return f;
		
	}

}
