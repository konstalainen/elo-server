package xyz.konstamaenpanen.logger;

import java.io.File;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Utilisation class.
 * 
 * @author Konsta Mäenpänen
 *
 */
public abstract class Utils {
	
	/**
	 * Get the timestamp of the current day in format dd.MM.yyyy HH:mm:ss.SSS for logging timestamp.
	 * 
	 * <p>For example:</p>
	 * <pre>07.03.2018 01:02:09.003</pre>
	 * @return timestamp in the above described format
	 */
	public static String getTimeStamp() {
		
		DateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS");
		return format.format(new Date());
		
	}
	
	/**
	 * Gets the date based prefix for the *.log filename.
	 * @return date prefix in format yyyyMMDD e.g. 20180307
	 */
	public static String getFileDatePrefix() {
		
		DateFormat format = new SimpleDateFormat("yyyyMMDD");
		return format.format(new Date());
		
	}
	
	/**
	 * Gets the root of the codebase.
	 * 
	 * <p>Root is where the code is executed (.jar or .class root).</p>
	 * 
	 * @return root path or empty string ("") if fails
	 */
	public static String getRoot() {
		
		try {
			
			return new File(Utils.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParent();
			
		} catch(SecurityException|NullPointerException|URISyntaxException|IllegalArgumentException e) {
			
			System.err.println(String.format("[%s]\tFailed to get root folder", getTimeStamp()));
			return "";
		}		
	}
}
